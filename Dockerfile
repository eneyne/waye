FROM debian:9 as buildo

WORKDIR /usr/local/

RUN apt update && apt install -y vim wget git gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev build-essential lua5.1 liblua5.1-0 liblua5.1.0-dev

RUN git clone http://luajit.org/git/luajit-2.0.git && cd luajit-2.0 && make && make install && ln -s /usr/local/lib/libluajit-5.1.so.2 /usr/lib/libluajit-5.1.so.2

RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar xvfz v0.3.1.tar.gz 
RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.20.tar.gz && tar xvfz v0.10.20.tar.gz 

RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.22.tar.gz && tar xvfz v0.1.22.tar.gz && cd lua-resty-core-0.1.22 && make install  PREFIX=/usr/local
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.12.tar.gz && tar xvfz v0.12.tar.gz && cd lua-resty-lrucache-0.12 && make install PREFIX=/usr/local

RUN ldconfig && export LUAJIT_LIB=/usr/lib && export LUAJIT_INC=/usr/local/luajit-2.0/src && wget https://nginx.org/download/nginx-1.22.0.tar.gz && tar xvfz nginx-1.22.0.tar.gz && cd nginx-1.22.0 && ./configure --with-ld-opt="-Wl,-rpath,$LUAJIT_LIB" --with-pcre --add-dynamic-module=../lua-nginx-module-0.10.20 --add-dynamic-module=../ngx_devel_kit-0.3.1 && make && make install



FROM debian:9
WORKDIR /usr/local/
COPY --from=buildo /usr/local/ .
WORKDIR /usr/lib/
COPY --from=buildo /usr/lib/ .
WORKDIR /usr/local/nginx/sbin
COPY --from=buildo /usr/local/nginx/sbin/nginx .
RUN chmod +x nginx
RUN groupadd --gid 999 nginx && useradd --uid 999 --gid 999 --no-create-home nginx
CMD ["./nginx", "-g", "daemon off;"]
